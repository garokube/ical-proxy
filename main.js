
const request = require('request');
const express = require('express');
const process = require('process');
const ical = require('node-ical');
const Prometheus = require('prom-client');

/**
 * kubectl -n infra create secret generic ical-proxy --from-literal=garo_work_secret=...  --from-literal=garo_work_calendar=...
 */
// This currently supports just a single feed at a time
const calendar_url = process.env.CALENDAR_URL;
const calendar_secret = process.env.CALENDAR_SECRET;

const refresh_time_seconds = 15 * 60;

console.log("ical-proxy starting.");
console.log("calendar url:", calendar_url);
console.log("calendar secret:", calendar_secret);

const ical_events = new Prometheus.Gauge({
    name: 'ical_events',
    help: 'How many events in the ical feed',
    labelNames: ['feed']
});
const ical_queries = new Prometheus.Counter({
    name: 'ical_queries_total',
    help: 'Number of queries in the feed',
    labelNames: ['feed']
});

var calendar = null; // Cache the .ical body 
var calendar_ical = null; // Cache the parsed node-ical structure.

function reload() {
    request(calendar_url, (err, response, body) => {
        if (err) {
            console.log("Error while fetching calendar:", err);
            return;
        }
        calendar = body;
        ical.async.parseICS(body, (err, data) => {     
            if (err) {
                console.error(err);
                return;
            }
            let events = 0;
            let non_events = 0;
            for (const id in data) {
                const event = data[id];
                if (event.type != 'VEVENT') {
                    non_events++;
                    continue;
                }

                events++;
                //console.log("Summary: " + event.summary + ", start date: " + event.start.toISOString());
            }
            calendar_ical = data;
            console.log("Total", events, "events", "and", non_events, "non-events");
            ical_events.labels(calendar_secret).set(events);
        });
    })
    setTimeout(reload, refresh_time_seconds * 1000);
}
reload();

function logRequest(req) {
    const ts = new Date();
    console.log(ts.toISOString() + "\t" + req.method + "\t" + req.url + "\t" + req.headers['user-agent']);
}
const app = express();

app.get('/', (req, res) => {
    logRequest(req);
    res.send("nothing here");
});

app.get('/' + calendar_secret, (req, res) => {
    logRequest(req);
    ical_queries.labels(calendar_secret).inc(1);
    res.setHeader("Cache-Control", "public, max-age=" + refresh_time_seconds);
    res.setHeader("Expires", new Date(Date.now() + refresh_time_seconds*1000).toUTCString());
    res.setHeader("Content-Type", "text/calendar; charset=utf-8");
    res.status(200);
    res.send(calendar);
});
app.get('/metrics', function(_, res) {
    res.type(Prometheus.register.contentType);
    res.send(Prometheus.register.metrics());
});

const server = app.listen(8000, () => {
  console.log(`Express is running on port ${server.address().port}`);
});
